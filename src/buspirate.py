import re
import types

from serial import Serial


class BusPirateException(Exception):
    pass

class BusPirate(object):
    _ver_re = {
        'hw': re.compile('Bus Pirate v(?P<version>\d\.[\d\w])'),
        'fw': re.compile('Firmware v(?P<version>\d\.[\d\w])'),
        'boot': re.compile('Bootloader v(?P<version>\d\.[\d\w])'),
    }

    def __init__(self, port):
        self.port = Serial(port, 115200, timeout=0.01)
        self.versions = {}
        self.reset()

    def __enter__(self):
        self.reset()
        return self

    def __exit__(self, type, value, tb):
        self.close()

    def reset(self):
        self._reset_to_terminal()
        self._bbio()

    def close(self):
        try:
            self._reset_to_terminal()
        finally:
            self.port.close()

    def _reset_to_terminal(self):
        self._discard()
        self._bbio()
        self._write_read(0x0F)
        data = self._write_read(b'#\n')
        if not 'HiZ>' in data:
            raise BusPirateException('Failed to reset BusPirate to terminal mode')
        self._parse_version(data)

    def _bbio(self):
        attempt = 0
        response = ''
        while response != b'BBIO':
            if attempt > 25:
                raise BusPirateException('Failed to enter BBIO mode')
            attempt += 1
            response = self._write_read(0x0, 4)
        version = self._read_str(1)
        if version != '1':
            raise BusPirateException('Invalid BBIO version. {0} received, but only 1 is supported.'.format(version))

    def _write(self, data):
        if isinstance(data, types.IntType):
            #print('-> {0:x} {0:d}'.format(data))
            self.port.write(chr(data))
        elif isinstance(data, types.StringTypes):
            #print('-> {0:s} "{1:s}"'.format(data, data.encode('hex')))
            self.port.write(data)
        elif isinstance(data, types.ListType):
            for i in data:
                self._write(i)

    def _read_byte(self):
        byte = ord(self.port.read(1))
        #print('<- {0:s} {1:d}'.format(chr(byte).encode('hex'), byte))
        return byte

    def _read_str(self, len=10000):
        data = self.port.read(len)
        #print('<- {0:s} "{1:s}"'.format(data, data.encode('hex')))
        return data

    def _write_read(self, data, read=1000):
        self._write(data)
        return self._read_str(read)

    def _write_read1(self, data):
        self._write(data)
        return self._read_byte()

    def _discard(self):
        if self.port.inWaiting() > 0:
            self._read_str()

    def _expect(self, data, expected, exception=None):
        ok = self._write_read(data, len(expected))
        if ok != expected:
            if exception:
                raise BusPirateException(exception)
            raise BusPirateException('Unexpected answer: want {0!r}, got {1!r}'.format(expected, ok))

    def _parse_version(self, data):
        lines = [l for l in data.split('\r\n') if ' v' in l]
        self.versions = {'hw': None, 'fw': None, 'boot': None}

        for id, re in self._ver_re.iteritems():
            for l in lines:
                m = re.search(l)
                if m:
                    self.versions[id] = m.groupdict()['version']
        if self.versions.values().count(None) > 0:
            raise BusPirateException('Failed to parse version info')


class BusPirate1Wire(BusPirate):
    _1W_RESET = 0x02
    _1W_READ  = 0x04
    _1W_SEARCH= 0x08
    _1W_ALARM = 0x09
    _1W_WRITE = 0x10
    _1W_CONFIG= 0x40

    def __init__(self, *args, **kwargs):
        super(BusPirate1Wire, self).__init__(*args, **kwargs)

    def reset(self, *args, **kwargs):
        super(BusPirate1Wire, self).reset(*args, **kwargs)

        if self._write_read(0x04) != b'1W01':
            raise BusPirateException('Failed to enter 1Wire mode')

    def configure(self, power=True, pullups=True, aux=True, cs=True):
        cmd = self._1W_CONFIG
        if power:
            cmd |= 1<<3
        if pullups:
            cmd |= 1<<2
        if aux:
            cmd |= 1<<1
        if cs:
            cmd |= 1
        ok = self._cmd(cmd, 'Failed to configure peripherals')


    def rom_search(self, alarm=False):
        if not alarm:
            ok = self._cmd(self._1W_SEARCH, 'Failed to perform ROM SEARCH macro')
        else:
            ok = self._cmd(self._1W_ALARM, 'Failed to perform ALARM SEARCH macro')

        addrs = []
        while True:
            addr = self._read_str(8)
            if addr == '\xFF'*8:
                break
            if addr:
                addrs.append(addr)
        return addrs

    def rom_read(self):
        self.write(b'\x33')
        return self.read(8)

    def rom_skip(self, cmd):
        self.write(b'\xCC'+cmd)

    def rom_match(self, addr, cmd):
        self.write(b'\x55'+addr+cmd)

    def write(self, data):
        if len(data) > 16:
            raise BusPirateException('Data is too long')
        self._cmd(self._1W_RESET, 'Failed to perform 1Wire RESET')
        self._write_read1(self._1W_WRITE | (len(data)-1))
        for i in data:
            self._cmd(i, 'Failed to perform 1Wire byte write')

    def read(self, len=1):
        data = []
        while len:
            len -= 1
            data.append(self._write_read1(self._1W_READ))
        return data


    def _cmd(self, cmd, exception):
        self._expect(cmd, '\x01', exception)

